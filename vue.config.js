module.exports = {
  css: {
    extract: false
  },
  pwa: {
    name: 'Brendan Mullins',
    themeColor: '#ffff33',
    backgroundColor: '#000000',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',

    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'src/sw.js'
      // ...other Workbox options...
    }
  },
  chainWebpack: config => {
    // markdown loader
    config.module
      .rule('html')
      .test(/\.md$/)
      .use('html-loader')
      .loader('html-loader')
      .end()
    config.module
      .rule('md')
      .test(/\.md$/)
      .use('markdown-loader')
      .loader('markdown-loader')
      .end()
  }
}
