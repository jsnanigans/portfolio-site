import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('./views/Welcome.vue')
    },
    {
      path: '/about',
      name: 'About',
      component: () => import('./views/About.vue')
    },
    {
      path: '/skills',
      name: 'Skills',
      component: () => import('./views/Skills.vue')
    },
    {
      path: '/open-source',
      name: 'OpenSource',
      component: () => import('./views/OpenSource.vue')
    },
    {
      path: '/contact',
      name: 'Contact',
      component: () => import('./views/Contact.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  const el = document.getElementById('content')

  if (!el) {
    next()
    return
  }

  el.scrollTo(0, 0)
  setTimeout(() => {
    next()
  }, el.scrollY * 0.7)
})

router.afterEach((to, from) => {
  if (from.name) {
    const content = document.getElementById('content')
    content.focus()
  }
  window.scrollTo(0, 0)
})

export default router
