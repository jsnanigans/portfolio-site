# About
I currently work as a Frontend Developer at LOOP in Salzburg.

Since the start of my career as a web developer nearly 8 years I went from creating iframe and table layout templates all the way to grid layout, from static html and css sites to single page apps and while the web has been evolving like no other platform I have been following it eagerly. Looking back at the state of the web just those few years ago when I started its amazing how much has changed — this is what I love about the web, there is always new APIs and libraries to learn. The feeling when you understand something new and get ideas on how to create awesome things with it, is exhilarating to me.

Some APIs Im really looking forward to are
<a tabindex="0" href="https://github.com/KenjiBaheux/portals" target="_blank">Portals</a>,
<a tabindex="0" href="https://developers.google.com/web/updates/2018/11/signed-exchanges" target="_blank">Webpackaging</a>,
<a tabindex="0" href="https://developers.google.com/web/updates/2018/01/paintapi" target="_blank">Houdini paint</a> and
<a tabindex="0" href="https://developers.google.com/web/updates/2018/10/animation-worklet" target="_blank">Houdini animation</a>