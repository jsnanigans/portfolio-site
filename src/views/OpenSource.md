# Open Source

I would not be where I am today without Open Source. Almost everything I have learned, was from open source projects by reading through their source code. Although there are good reasons why to sometimes not open source a project, I think that everyone who benefits from OS should try to give back in some way or another, for that reason I release all my side projects that could be useful to other people on github or gitlab. OS is not just important for the world of IT, I think it's required in all areas of life to move the wheel of knowledge further.

Currently I maintain this small plugin I made for Vue.js <a href="https://github.com/jsnanigans/vue-parallax-js">vue-parallax-js</a>, and contribute to <a href="https://github.com/FullHuman/purgecss">purgecss</a> and its webpack plugin whenever I have time. Additionally I hang around the issue pages on some of my favorite projects and on Stack Overflow and help people out whenever I can.

Have a look at my <a href="https://github.com/jsnanigans">github</a> and <a href="https://gitlab.com/jsnanigans">gitlab</a> accounts to see all my other projects. This page you are viewing right now can be found on gitlab.